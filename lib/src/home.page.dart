import 'package:app_indicators/src/blocs/bottom_navigator/bottom_navigator_bloc.dart';
import 'package:app_indicators/src/pages/dashboard.page.dart';
import 'package:app_indicators/src/pages/period.page.dart';
import 'package:app_indicators/src/pages/indicators.page.dart';
import 'package:app_indicators/src/pages/workers.page.dart';
import 'package:app_indicators/src/helpers/app.bars.dart';
import 'package:app_indicators/src/widgets/bottom.navigator.widget.dart';
import 'package:app_indicators/src/widgets/drawer.navigator.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BottomNavigatorBloc, BottomNavigatorState>(
      builder: (context, state) {
        
        Widget mainPage = const DashboardPage();
        AppBar widgetAppBar = buildAppBarDashboard(context);
        
        switch (state.currentIndex) {
          case 0:
            mainPage = const DashboardPage();
            widgetAppBar = buildAppBarDashboard(context);
            break;
          case 1:
            mainPage = const IndicatorsPage();
            widgetAppBar = buildAppBarIndicators(context);
            break;
          case 2:
            mainPage = const WorkersPage();
            widgetAppBar = buildAppBarWorkers(context);
            break;
          case 3:
            mainPage = const PeriodPage();
            widgetAppBar = buildAppBarImport(context);
            break;
        }

        return Scaffold(
          drawer: const NavigatiomDrawer(),
          appBar: widgetAppBar,
          body: mainPage,
          bottomNavigationBar: const CustomBottomNavigationBar(),
        );
      },
    );
  }
}
