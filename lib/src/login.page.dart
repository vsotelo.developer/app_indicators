import 'package:app_indicators/src/blocs/login/login_bloc.dart';
import 'package:app_indicators/src/models/login.model.dart';
import 'package:app_indicators/src/models/user.dart';
import 'package:app_indicators/src/service/login.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decode/jwt_decode.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isDarkMode = SchedulerBinding.instance!.window.platformBrightness == Brightness.dark;
    final loginBloc = BlocProvider.of<LoginBloc>(context);

    const storage = FlutterSecureStorage();
    final LoginService loginService = LoginService();
    final TextEditingController userController = TextEditingController();

    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return Scaffold(
          body: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Visibility(visible: state.loanding, child: const LinearProgressIndicator()),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 50),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 300,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Bienvenido',
                            style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold, color: isDarkMode ? Colors.white : Colors.black),
                          ),
                          const Text(
                            '¿Estas listo para la experiencia?',
                            style: TextStyle(fontSize: 15, color: Colors.grey, fontWeight: FontWeight.bold),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 5),
                            child: TextFormField(
                              controller: userController,
                              decoration: const InputDecoration(hintText: 'Nombre de Usuario'),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 25),
                            child: ClipRRect(
                              borderRadius: const BorderRadius.all(Radius.circular(25)),
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 50,
                                decoration: BoxDecoration(
                                  color: ThemeData().primaryColor,
                                  borderRadius: const BorderRadius.all(Radius.circular(25)),
                                ),
                                child: MaterialButton(
                                  child: const Text(
                                    'Ingresar',
                                    style: TextStyle(fontSize: 15, color: Colors.white),
                                  ),
                                  onPressed: state.loanding
                                      ? null
                                      : () async {
                                          if (userController.text.trim() != '') {
                                            loginBloc.add(const ChangeFlagLoandingEvent(loanding: true));
            
                                            LoginResponse loginResponse = await loginService.login(userController.text);
            
                                            await storage.write(key: 'token', value: loginResponse.result.token);
                                            Map<String, dynamic> payload = Jwt.parseJwt(loginResponse.result.token);
                                            User user = User.fromMap(payload);
            
                                            loginBloc.add(SetUserTokenEvent(user: user));
                                            loginBloc.add(const ChangeFlagLoandingEvent(loanding: false));
            
                                            Navigator.pushReplacementNamed(context, 'dashboard');
                                          }
                                        },
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
