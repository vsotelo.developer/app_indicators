import 'package:app_indicators/src/helpers/http.interceptor.dart';
import 'package:app_indicators/src/helpers/utils.dart';
import 'package:app_indicators/src/models/period.average.response.dart';
import 'package:app_indicators/src/models/period.model.dart';
import 'package:app_indicators/src/models/period.register.request.dart';
import 'package:app_indicators/src/models/period.register.response.dart';
import 'package:app_indicators/src/models/perios.list.response.dart';
import 'package:app_indicators/src/models/update.indicator.request.dart';
import 'package:dio/dio.dart';

import '../models/average.indicators.response.dart';

class PeriodService {

  final Dio _dioPeriod;

  PeriodService() : _dioPeriod = Dio()..interceptors.add(HttpInterceptor());

  Future<List<Period>> getPeriods() async {
    String url = baseUrl + "/period/";
    final resp = await _dioPeriod.get(url);
    return PeriodListResponse.fromMap(resp.data).result;
  }

  Future<bool> updateMasiveIndicators(List<IndicatorValueUpdateRequest> listIndicatorsUpdate) async {
    String url = baseUrl + "/period/update/masive/";
    final res = await _dioPeriod.post(
      url,
      data: List<dynamic>.from(listIndicatorsUpdate.map((x) => x.toMap()))
    );
    return res.statusCode! == 200;
  }

  Future<PeriodRegisterResponse> registerNewPeriod(PeriodRegisterRequest request) async {
    String url = baseUrl + "/period/";
    final res = await _dioPeriod.post(
      url,
      data: request.toJson()
    );
    return PeriodRegisterResponse.fromMap(res.data);
  }

  Future<List<PeriodAverage>> getAveragePeriod() async {
    String url = baseUrl + "/period/average";
     final resp = await _dioPeriod.get(url);
    return PeriodAverageResponse.fromMap(resp.data).result;
  }

  Future<List<AverageIndicators>> getIndicatorsAverage({ required String periodId}) async {
  String url = baseUrl + "/period/average/indicators?periodId=" + periodId;
     final resp = await _dioPeriod.get(url);
    return AverageIndicatorsResponse.fromMap(resp.data).result;
  }
}