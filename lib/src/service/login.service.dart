
import 'package:app_indicators/src/helpers/utils.dart';
import 'package:app_indicators/src/models/login.model.dart';
import 'package:dio/dio.dart';

class LoginService {

  final Dio _dioLogin;
  LoginService() : _dioLogin = Dio();

  Future<LoginResponse> login(String username) async {
    String authURL = baseUrl + "/user/auth/?documentNumber=" + username;
    final resp = await _dioLogin.get(authURL);
    return LoginResponse.fromMap(resp.data);
  }

}