
import 'package:app_indicators/src/helpers/http.interceptor.dart';
import 'package:app_indicators/src/helpers/utils.dart';
import 'package:app_indicators/src/models/indicator.model.dart';
import 'package:app_indicators/src/models/indicators.response.dart';
import 'package:dio/dio.dart';

class IndicatorsService {

  final Dio _dioIndicators;

  IndicatorsService() : _dioIndicators = Dio()..interceptors.add(HttpInterceptor());

  Future<List<Indicator>> getListIndicators() async {
    String authURL = baseUrl + "/indicator/";
    final resp = await _dioIndicators.get(authURL);
    return UserIndicatorsResponse.fromMap(resp.data).result;
  }

  Future<List<Indicator>> saveIndicators(List<Indicator> indicators) async {
    String authURL = baseUrl + "/indicator/manage/";
    final res = await _dioIndicators.post(
      authURL,
      data: List<dynamic>.from(indicators.map((x) => x.toMap()))
    );
    return UserIndicatorsResponse.fromMap(res.data).result;
  }
}