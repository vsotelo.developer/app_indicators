
import 'package:app_indicators/src/helpers/http.interceptor.dart';
import 'package:app_indicators/src/helpers/utils.dart';
import 'package:app_indicators/src/models/worker.model.dart';
import 'package:app_indicators/src/models/workers.response.dart';
import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:io';
import 'package:http_parser/http_parser.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart' as http;

class WorkerService {

   final Dio _dioWorkers;

  WorkerService() : _dioWorkers = Dio()..interceptors.add(HttpInterceptor());

  Future<void> dowloandFormatWorker() async {
    
  }

  Future<List<Worker>> getWorkers() async {
    String authURL = baseUrl + "/worker/";
    final resp = await _dioWorkers.get(authURL);
    return WorkerResponse.fromMap(resp.data).result;    
  }

  Future<void> downloadFile() async {
    bool hasPermission = await _requestWritePermission();
    if (!hasPermission) return;
    File aaa = await descargar();
    OpenFile.open(aaa.path, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  }

  Future<bool> _requestWritePermission() async {
    await Permission.storage.request();
    return await Permission.storage.request().isGranted;
  }

  descargar() async {

    var url = Uri.https(host, "/business-indicator/worker/download/format/");
    const storage = FlutterSecureStorage();
    String? token = await storage.read(key: 'token');

    var headers = {
      'Authorization': token!,
      'Content-Type': 'application/json; charset=UTF-8',
    };

    http.Response resp = await http.get(url, headers: headers);

    if (resp.statusCode == 200) {
      final buffer = resp.bodyBytes.buffer;
      Directory tempDir = await getTemporaryDirectory();
      String tempPath = tempDir.path;
      var filePath = tempPath + '/workers.xlsx';
      return File(filePath).writeAsBytes(buffer.asUint8List(resp.bodyBytes.offsetInBytes, resp.bodyBytes.lengthInBytes));
    } else {
      return null;
    }
  }

  selectFile() async {
    final file = await FilePicker.platform.pickFiles(type: FileType.custom, allowedExtensions: ['xlsx', 'xls']);

    if (file != null) {
      PlatformFile? _platformFile = file.files.first;
      String? filePath = _platformFile.path;
      _uploadFile(File(filePath!));
    }
  }

  _uploadFile(File file) async {
    String fileName = file.path.split('/').last;
    FormData data = FormData.fromMap({
      "path": file.path,
      "file": await MultipartFile.fromFile(file.path, filename: fileName, contentType: MediaType("application", "vnd.ms-excel")),
    });
    String url = baseUrl + "/worker/import";
    await _dioWorkers.post(url, data: data);    
  }

}