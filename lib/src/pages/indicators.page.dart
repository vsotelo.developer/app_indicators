import 'package:animate_do/animate_do.dart';
import 'package:app_indicators/src/blocs/indicators/indicators_bloc.dart';
import 'package:app_indicators/src/models/indicator.model.dart';
import 'package:app_indicators/src/service/indicators.service.dart';
import 'package:app_indicators/src/widgets/message.empty.widget.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';

class IndicatorsPage extends StatelessWidget {
  const IndicatorsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    IndicatorsService indicatorsService = IndicatorsService();
    bool isDarkMode = SchedulerBinding.instance!.window.platformBrightness == Brightness.dark;
    Color color = isDarkMode ? Colors.white : Colors.black;
    final indicatorsBloc = BlocProvider.of<IndicatorsBloc>(context);

    TextEditingController controllerNameIndicator = TextEditingController();
    TextEditingController controllerValueIndicator = TextEditingController();

    return BlocBuilder<IndicatorsBloc, IndicatorsState>(
      builder: (context, state) {
        return RefreshIndicator(
          onRefresh: () async {
            indicatorsBloc.add(const SetLoandingIndicatorsEvent(loandingIndicators: true));
            indicatorsBloc.setListIndicators(await indicatorsService.getListIndicators());
            indicatorsBloc.add(const SetLoandingIndicatorsEvent(loandingIndicators: false));
          },
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Nombre de Indicador (Ejemplo: Responsabilidad)',
                    style: TextStyle(color: color, fontSize: 16, fontWeight: FontWeight.w800),
                  ),
                  const SizedBox(height: 5),
                  TextFormField(
                    style: TextStyle(color: color),
                    controller: controllerNameIndicator,
                    decoration: InputDecoration(
                      hintText: 'Ingresa Nombre',
                      fillColor: color,
                      hoverColor: color,
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.add_circle_outline),
                        onPressed: () {
                          if (controllerNameIndicator.text.trim() != '') {
                            indicatorsBloc.add(const IsSetPorsentaje(isSetPorcentaje: true));
                          }
                        },
                      ),
                    ),
                  ),
                  Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 300,
                          child: state.loandingIndicators
                              ? const Center(child: CircularProgressIndicator())
                              : PieChart(
                                  PieChartData(
                                    sectionsSpace: 2,
                                    centerSpaceRadius: 0,
                                    borderData: FlBorderData(
                                      show: false,
                                    ),
                                    sections: state.indicators
                                        .map((indicator) => PieChartSectionData(
                                              color: Color(int.parse(indicator.hexColor)),
                                              value: indicator.equivalentPercentage.toDouble(),
                                              title: indicator.equivalentPercentage.toStringAsFixed(2).toString() + "%",
                                              radius: 140.0,
                                              titleStyle: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.white),
                                              badgePositionPercentageOffset: .98,
                                            ))
                                        .toList(),
                                  ),
                                  swapAnimationDuration: const Duration(milliseconds: 150),
                                  swapAnimationCurve: Curves.linear,
                                ),
                        ),
                      ),
                      Visibility(visible: state.indicators.isEmpty, child: const MessageEmpty(message: 'No tienes indicadores registrados.')),
                      Visibility(
                          visible: state.isSetPorcentaje,
                          child: Center(
                            child: FadeInUp(
                              duration: const Duration(milliseconds: 100),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Container(
                                  padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                                  width: MediaQuery.of(context).size.width,
                                  height: 140,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.only(bottomLeft: Radius.circular(25), bottomRight: Radius.circular(25)),
                                      boxShadow: [BoxShadow(color: Colors.black.withOpacity(0.3), offset: const Offset(0, 5), blurRadius: 5)]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    children: [
                                      GestureDetector(
                                        child: const SizedBox(
                                          child: Icon(Icons.cancel, color: Colors.red),
                                        ),
                                        onTap: () {
                                          controllerNameIndicator.text = '';
                                          indicatorsBloc.add(const IsSetPorsentaje(isSetPorcentaje: false));
                                        },
                                      ),
                                      SizedBox(
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          children: [
                                            SizedBox(
                                              width: 100,
                                              //height: 100,
                                              child: TextFormField(
                                                controller: controllerValueIndicator,
                                                maxLength: 3,
                                                style: TextStyle(color: color),
                                                inputFormatters: <TextInputFormatter>[
                                                  FilteringTextInputFormatter.digitsOnly,
                                                ],
                                                keyboardType: TextInputType.number,
                                                decoration: InputDecoration(
                                                  hintText: 'Porcentaje',
                                                  counterText: '',
                                                  fillColor: color,
                                                  hoverColor: color,
                                                ),
                                              ),
                                            ),
                                            ClipRRect(
                                              borderRadius: const BorderRadius.all(Radius.circular(25)),
                                              child: Container(
                                                width: 200,
                                                height: 40,
                                                decoration:
                                                    BoxDecoration(color: state.color, borderRadius: const BorderRadius.all(Radius.circular(25))),
                                                child: MaterialButton(
                                                    child: const Text('Cambiar Color', style: TextStyle(color: Colors.white)),
                                                    onPressed: () {
                                                      showDialog(
                                                        context: context,
                                                        builder: (BuildContext context) {
                                                          return AlertDialog(
                                                            titlePadding: const EdgeInsets.all(0),
                                                            contentPadding: const EdgeInsets.all(0),
                                                            content: SingleChildScrollView(
                                                              child: MaterialPicker(
                                                                pickerColor: Colors.green,
                                                                onColorChanged: (color) {
                                                                  indicatorsBloc.add(ChangeColorIndicatorEvent(color: color));
                                                                  Navigator.pop(context);
                                                                },
                                                                enableLabel: true,
                                                                portraitOnly: true,
                                                              ),
                                                            ),
                                                          );
                                                        },
                                                      );
                                                    }),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 10),
                                      ClipRRect(
                                        borderRadius: const BorderRadius.all(Radius.circular(25)),
                                        child: Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: 40,
                                          decoration: BoxDecoration(
                                              color: ThemeData().primaryColor, borderRadius: const BorderRadius.all(Radius.circular(25))),
                                          child: MaterialButton(
                                              child: const Text('Agregar', style: TextStyle(color: Colors.white)),
                                              onPressed: () {
                                                if (controllerValueIndicator.text.trim() != '') {
                                                  Indicator indicator = Indicator(
                                                      id: '',
                                                      equivalentPercentage: double.parse(controllerValueIndicator.text),
                                                      hexColor: state.color.toString().replaceAll('Color(', '').replaceAll(')', ''),
                                                      name: controllerNameIndicator.text);

                                                  List<Indicator> indicators = state.indicators;
                                                  List<Indicator> newIntanceIndicators = [];

                                                  for (var element in indicators) {
                                                    newIntanceIndicators.add(element);
                                                  }

                                                  newIntanceIndicators.add(indicator);
                                                  indicatorsBloc.setListIndicators(newIntanceIndicators);

                                                  controllerNameIndicator.text = '';
                                                  indicatorsBloc.add(const IsSetPorsentaje(isSetPorcentaje: false));
                                                  FocusManager.instance.primaryFocus?.unfocus();
                                                }
                                              }),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ))
                    ],
                  ),
                  ...state.indicators
                      .map((indicator) => Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5),
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 50,
                              decoration: BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(25)), border: Border.all()),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Icon(Icons.circle, color: Color(int.parse(indicator.hexColor))),
                                    Text(indicator.name, style: TextStyle(fontWeight: FontWeight.bold, color: color)),
                                    Text(indicator.equivalentPercentage.toString() + "%",
                                        style: TextStyle(fontWeight: FontWeight.bold, color: color)),
                                    IconButton(
                                      icon: const Icon(Icons.delete, color: Colors.red),
                                      onPressed: () {
                                        List<Indicator> indicators = state.indicators;
                                        List<Indicator> newIntanceIndicators = [];
                                        for (var element in indicators) {
                                          newIntanceIndicators.add(element);
                                        }
                                        newIntanceIndicators.remove(indicator);
                                        indicatorsBloc.setListIndicators(newIntanceIndicators);
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ))
                      .toList(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
