import 'package:app_indicators/src/blocs/period/period_bloc.dart';
import 'package:app_indicators/src/helpers/app.bars.dart';
import 'package:app_indicators/src/models/period.detail.dart';
import 'package:app_indicators/src/models/update.indicator.request.dart';
import 'package:app_indicators/src/models/worker.detail.period.model.dart';
import 'package:app_indicators/src/widgets/radial.progress.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class DetailPeriodPage extends StatelessWidget {
  const DetailPeriodPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final periodBloc = BlocProvider.of<PeriodBloc>(context);
    final DateFormat formatter = DateFormat('yyyy-MM-dd');

    return BlocBuilder<PeriodBloc, PeriodState>(
      builder: (context, state) {
        return Scaffold(
          appBar: buildAppBarDetailPeriod(context),
          body: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
              child: Column(
                children: [
                  // Resumen
                  const Divider(color: Colors.grey),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Text('Promedio Ponderado del Día: ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17)),
                            Text("Fecha: " + formatter.format(state.onPeriodDetailSelected!.date),
                                style: const TextStyle(color: Colors.grey, fontWeight: FontWeight.bold)),
                          ],
                        ),
                        SizedBox(width: 100, height: 100, child: RadialProgress(porcentaje: state.onPeriodDetailSelected!.averageByDays))
                      ],
                    ),
                  ),
                  const Divider(color: Colors.grey),
                  ExpansionPanelList(
                      animationDuration: const Duration(milliseconds: 500),
                      elevation: 1,
                      children: [
                        ...state.onPeriodDetailSelected!.workerDetailPeriods
                            .map(
                              (workerDetailPeriod) => ExpansionPanel(
                                canTapOnHeader: true,
                                isExpanded: workerDetailPeriod.getSelected,
                                headerBuilder: (context, isExpanded) {
                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(workerDetailPeriod.worker.name, style: const TextStyle(fontWeight: FontWeight.bold)),
                                            Text(workerDetailPeriod.worker.documentNumber),
                                          ],
                                        ),
                                        Text(workerDetailPeriod.worker.email, style: const TextStyle(color: Colors.grey)),
                                      ],
                                    ),
                                  );
                                },
                                body: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          const Text('Promedio de desenpeño Total: ', style: TextStyle(fontWeight: FontWeight.bold)),
                                          Text(workerDetailPeriod.valueAverage.toStringAsFixed(2).toString() + " %", style: const TextStyle(fontWeight: FontWeight.bold))
                                        ],
                                      ),
                                      const Divider(color: Colors.black, thickness: 2),
                                      ...workerDetailPeriod.indicatorValues
                                          .map(
                                            (indicatorValue) => Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              children: [
                                                Icon(Icons.circle, color: Color(int.parse(indicatorValue.indicator.hexColor))),
                                                Text(indicatorValue.indicator.name, style: const TextStyle()),
                                                Text(
                                                  indicatorValue.getNewValue != null ?  indicatorValue.getNewValue.toString() + " %"
                                                  : indicatorValue.value.toString() + " %",
                                                  style: const TextStyle()
                                                ),
                                                IconButton(
                                                  icon: const FaIcon(FontAwesomeIcons.filePen, color: Colors.lightBlue),
                                                  onPressed: () async {
                                                    periodBloc.add(OnWorkerDetailPeriodSelectedEvent(onWorkerDetailPeriodSelected: workerDetailPeriod));
                                                    periodBloc.add(OnIndicatorValueSelectedEvent(onIndicatorValueSelected: indicatorValue));                                                    
                                                    TextEditingController controller = TextEditingController();
                                                    await dialogInsertarPeriodo(context, controller);
                                                  },
                                                )
                                              ],
                                            ),
                                          )
                                          .toList(),
                                    ],
                                  ),
                                ),
                              ),
                            )
                            .toList()
                      ],
                      expansionCallback: (panelIndex, isExpanded) {
                        WorkerDetailPeriod workerDetailPeriod = state.onPeriodDetailSelected!.workerDetailPeriods[panelIndex];
                        workerDetailPeriod.setSelected = !isExpanded;

                        PeriodDetail onPeriodDetailSelected = state.onPeriodDetailSelected!;
                        PeriodDetail periodDetailClone = PeriodDetail(
                            averageByDays: onPeriodDetailSelected.averageByDays,
                            date: onPeriodDetailSelected.date,
                            id: onPeriodDetailSelected.id,
                            workerDetailPeriods: onPeriodDetailSelected.workerDetailPeriods);

                        periodBloc.add(OnPeriodDetailSelectedEvent(onPeriodDetailSelected: periodDetailClone));
                      }),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  dialogInsertarPeriodo(BuildContext context, TextEditingController controller) {
      return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return BlocBuilder<PeriodBloc, PeriodState> (
            builder: (context, state) {
              controller.text  = state.onIndicatorValueSelected!.value.toString().replaceAll('.0', '');
              final periodBloc = BlocProvider.of<PeriodBloc>(context);
              return AlertDialog(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const FaIcon(FontAwesomeIcons.user),
                    const SizedBox(width: 10),
                    Flexible(
                      child: Text(state.onWorkerDetailPeriodSelected!.worker.name,
                          style: const TextStyle(fontSize: 20), maxLines: 2, overflow: TextOverflow.ellipsis),
                    ),
                    GestureDetector(child: const FaIcon(Icons.cancel, color: Colors.red), onTap: () => Navigator.pop(context))
                  ],
                ),
                shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                clipBehavior: Clip.antiAliasWithSaveLayer,
                content: StatefulBuilder(builder: (context, setState) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(state.onIndicatorValueSelected!.indicator.name , style: const TextStyle(fontWeight: FontWeight.bold)),
                        TextField(
                          autofocus: true,
                          controller: controller,
                          maxLength: 3,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly,
                          ],
                          keyboardType: TextInputType.number,
                          decoration: const InputDecoration(                              
                              hintText: "Valor",
                              hintStyle: TextStyle(color: Color(0xffBFBFBF)),
                              counterText: ''),
                        ),
                        const SizedBox(height: 20),
                        ClipRRect(
                          borderRadius: const BorderRadius.all(Radius.circular(25)),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 50,
                            decoration: const BoxDecoration(color: Colors.lightBlue, borderRadius: BorderRadius.all(Radius.circular(25))),
                            child: MaterialButton(
                              child: const Text('Actualizar', style: TextStyle(color: Colors.white, fontSize: 16)),
                              onPressed: () async {
                                state.onIndicatorValueSelected!.setNewValue = double.parse(controller.text);
                                IndicatorValueUpdateRequest request = IndicatorValueUpdateRequest(
                                    periodId: state.onPeriodSelected!.id,
                                    periodDetailId: state.onPeriodDetailSelected!.id,
                                    indicatorValueId: state.onIndicatorValueSelected!.id,
                                    workerPeriodDetailId: state.onWorkerDetailPeriodSelected!.id,
                                    value: double.parse(controller.text),
                                );
                                periodBloc.add(OnAddIndicatorValueUpdate(indicatorValueUpdateRequest: request));
                                Navigator.pop(context);
                              },
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                }),
              );
            },
          );
        });
  }
}
