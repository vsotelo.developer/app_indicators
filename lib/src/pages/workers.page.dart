import 'package:app_indicators/src/blocs/worker/worker_bloc.dart';
import 'package:app_indicators/src/service/workers.service.dart';
import 'package:app_indicators/src/widgets/message.empty.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WorkersPage extends StatelessWidget {
  const WorkersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final workerBloc = BlocProvider.of<WorkerBloc>(context);
    WorkerService workerService = WorkerService();

    return BlocBuilder<WorkerBloc, WorkerState>(
      builder: (context, state) {
        return RefreshIndicator(
          onRefresh: () async {
              workerBloc.add(const OnLoandingWorkersEvent(loandingWorkers: true));    
              workerBloc.add(SetWorkersListEvent(workers: await workerService.getWorkers()));    
              workerBloc.add(const OnLoandingWorkersEvent(loandingWorkers: false));
          },
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              children: [
                state.loandingWorkers ? const LinearProgressIndicator() : const SizedBox(),
                const SizedBox(height: 15),
                Visibility(
                  visible: state.workers.isEmpty,
                  child: const MessageEmpty(message: 'No tienes Trabajadores registrados.')
                ),
                ...state.workers.map((worker) => Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      Container(
                          padding: const EdgeInsets.symmetric(horizontal: 15),
                          width: MediaQuery.of(context).size.width,
                          height: 70,                      
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  const FaIcon(FontAwesomeIcons.userCheck),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(worker.name, style: const TextStyle(fontWeight: FontWeight.bold)),
                                      Text(worker.email, style: const TextStyle(color: Colors.grey))
                                    ],
                                  ),
                                  Text(worker.documentNumber)
                                ],
                              ),                          
                            ],
                          ),
                        ),
                        const Divider(color: Colors.grey)
                    ],
                  ),
                )              
                ).toList(),
              ],
            ),
          ),
        );
      },
    );
  }
}
