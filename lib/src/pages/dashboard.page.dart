import 'package:app_indicators/src/blocs/dashboard/dashboard_bloc.dart';
import 'package:app_indicators/src/models/average.indicators.response.dart';
import 'package:app_indicators/src/models/period.average.response.dart';
import 'package:app_indicators/src/service/period.service.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DashboardPage extends StatelessWidget {
  const DashboardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DashboardBloc dashboardBloc = BlocProvider.of<DashboardBloc>(context);
    PeriodService periodService = PeriodService();

    return BlocBuilder<DashboardBloc, DashboardState>(
      builder: (context, state) {
        return RefreshIndicator(
          onRefresh: () async {
            dashboardBloc.add(const OnSeLoandingAverage(loandingListAverage: true));
            List<PeriodAverage> listPeriodsAverage = await periodService.getAveragePeriod();
            dashboardBloc.add(OnChangePeriodAverageEvent(listPeriodsAverage: listPeriodsAverage));
            dashboardBloc.add(const OnSeLoandingAverage(loandingListAverage: false));
          },
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              children: [
                Visibility(visible: state.loandingListAverage, child: const LinearProgressIndicator()),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [                      
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: const [                                
                                Text('Colaborador:', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
                                SizedBox(width: 5),
                                Text('Todos los Colaboradores', style: TextStyle(fontSize: 15)),
                              ],
                            ),
                            IconButton(icon: FaIcon(FontAwesomeIcons.trash, color: ThemeData().primaryColor), onPressed: () {})
                          ],
                        ),
                      ),
                      Column(
                        children: [
                          state.listPeriodsAverage.isEmpty ? const SizedBox() : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,                 
                            children: const [
                              Divider(color: Colors.black, thickness: 1),
                              Text('Resumen de Periodos', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                              SizedBox(height: 5),
                              _ChartPeriodAverage(),
                            ],
                          ),
                          //const SizedBox(height: 20),
                          state.listAverageIndicators.isEmpty ? const SizedBox() : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const Divider(color: Colors.black, thickness: 1),
                              Row(
                                children: [
                                  const Text('Indicadores por Periodo: ', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                                  Text(state.namePeriodSelected, style: const TextStyle(fontSize: 15)),
                                ],
                              ),
                              const SizedBox(height: 5),
                              const _ChartPeriodAverageIndicator(),
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class _ChartPeriodAverage extends StatelessWidget {
  const _ChartPeriodAverage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PeriodService periodService = PeriodService();
    DashboardBloc dashboardBloc = BlocProvider.of<DashboardBloc>(context);
    return BlocBuilder<DashboardBloc, DashboardState>(
      builder: (context, state) {
        return Container(
          padding: const EdgeInsets.only(top: 20),
          width: MediaQuery.of(context).size.width,
          height: 220,
          decoration: BoxDecoration(color: ThemeData().primaryColor.withOpacity(0.2), borderRadius: const BorderRadius.all(Radius.circular(20))),
          child: BarChart(
            BarChartData(
              barTouchData: BarTouchData(
                touchTooltipData: BarTouchTooltipData(
                  tooltipBgColor: Colors.blueGrey,
                  getTooltipItem: (group, groupIndex, rod, rodIndex) {
                    return BarTooltipItem(
                      state.listPeriodsAverage[group.x.toInt()].periodName + '\n',
                      const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: (rod.toY).toString(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    );
                  },
                ),
                touchCallback: (FlTouchEvent flTouchEvent, BarTouchResponse? barTouchResponse) async {
                  if (flTouchEvent is FlPointerHoverEvent) {
                    if (barTouchResponse != null && barTouchResponse.spot != null) {
                      dashboardBloc.add(const OnSeLoandingAverage(loandingListAverage: true));

                      BarTouchedSpot spot = barTouchResponse.spot!;
                      PeriodAverage periodAverage = state.listPeriodsAverage[spot.touchedBarGroupIndex];

                      dashboardBloc.add(OnSetPeriodNameSelected(namePeriodSelected: periodAverage.periodName));

                      List<AverageIndicators> listAverageIndicators = await periodService.getIndicatorsAverage(periodId: periodAverage.periodId);

                      dashboardBloc.add(OnChangeIndicatorsAverageEvent(listAverageIndicators: listAverageIndicators));

                      dashboardBloc.add(const OnSeLoandingAverage(loandingListAverage: false));
                    }
                  }
                },
              ),
              titlesData: FlTitlesData(
                show: true,
                rightTitles: AxisTitles(
                  sideTitles: SideTitles(showTitles: false),
                ),
                topTitles: AxisTitles(
                  sideTitles: SideTitles(showTitles: false),
                ),
                bottomTitles: AxisTitles(
                  sideTitles: SideTitles(
                    showTitles: true,
                    getTitlesWidget: (double value, TitleMeta meta) {
                      const style = TextStyle(
                        color: Colors.black,
                        //fontWeight: FontWeight.bold,
                        fontSize: 14,
                      );

                      return Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Text(state.listPeriodsAverage[value.toInt()].periodName.substring(0, 3), style: style));
                    },
                    reservedSize: 38,
                  ),
                ),
                leftTitles: AxisTitles(
                  sideTitles: SideTitles(
                    showTitles: false,
                  ),
                ),
              ),
              borderData: FlBorderData(
                show: false,
              ),
              barGroups:
                  state.listPeriodsAverage.map((e) => makeGroupData(e.index, e.value, barColor: ThemeData().primaryColor, isTouched: false)).toList(),
              gridData: FlGridData(show: false),
            ),
            swapAnimationDuration: const Duration(milliseconds: 150), // Optional chartData BarChartGroupData
            swapAnimationCurve: Curves.linear, // Optional
          ),
        );
      },
    );
  }
}

class _ChartPeriodAverageIndicator extends StatelessWidget {
  const _ChartPeriodAverageIndicator({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<DashboardBloc, DashboardState>(
      builder: (context, state) {
        return Container(
          padding: const EdgeInsets.only(top: 20),
          width: MediaQuery.of(context).size.width,
          height: 220,
          decoration: BoxDecoration(color: ThemeData().primaryColor.withOpacity(0.2), borderRadius: const BorderRadius.all(Radius.circular(20))),
          child: BarChart(
            BarChartData(
              barTouchData: BarTouchData(
                touchTooltipData: BarTouchTooltipData(
                  tooltipBgColor: Colors.blueGrey,
                  getTooltipItem: (group, groupIndex, rod, rodIndex) {
                    return BarTooltipItem(
                      state.listAverageIndicators[group.x.toInt()].indicator.name + '\n',
                      const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                      children: <TextSpan>[
                        TextSpan(
                          text: (rod.toY).toString(),
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    );
                  },
                ),
                touchCallback: (_, __) {},
              ),
              titlesData: FlTitlesData(
                show: true,
                rightTitles: AxisTitles(
                  sideTitles: SideTitles(showTitles: false),
                ),
                topTitles: AxisTitles(
                  sideTitles: SideTitles(showTitles: false),
                ),
                bottomTitles: AxisTitles(
                  sideTitles: SideTitles(
                    showTitles: true,
                    getTitlesWidget: (double value, TitleMeta meta) {
                      const style = TextStyle(
                        color: Colors.black,
                        //fontWeight: FontWeight.bold,
                        fontSize: 14,
                      );

                      return Padding(
                          padding: const EdgeInsets.only(top: 16),
                          child: Text(state.listAverageIndicators[value.toInt()].indicator.name.substring(0, 3), style: style));
                    },
                    reservedSize: 38,
                  ),
                ),
                leftTitles: AxisTitles(
                  sideTitles: SideTitles(
                    showTitles: false,
                  ),
                ),
              ),
              borderData: FlBorderData(
                show: false,
              ),
              barGroups: state.listAverageIndicators
                  .map((e) => makeGroupData(e.index, e.average, barColor: Color(int.parse(e.indicator.hexColor)), isTouched: false))
                  .toList(),
              gridData: FlGridData(show: false),
            ),
            swapAnimationDuration: const Duration(milliseconds: 150), // Optional chartData BarChartGroupData
            swapAnimationCurve: Curves.linear, // Optional
          ),
        );
      },
    );
  }
}

BarChartGroupData makeGroupData(
  int x,
  double y, {
  bool isTouched = false,
  Color barColor = Colors.red,
  double width = 22,
  List<int> showTooltips = const [],
}) {
  return BarChartGroupData(
    x: x,
    barRods: [
      BarChartRodData(
        toY: y,
        color: barColor,
        width: width,
        borderSide: isTouched ? const BorderSide(color: Colors.white, width: 3) : const BorderSide(color: Colors.white, width: 0),
        backDrawRodData: BackgroundBarChartRodData(
          show: true,
          toY: 100,
          color: ThemeData().primaryColor.withOpacity(0.3),
        ),
      ),
    ],
    showingTooltipIndicators: showTooltips,
  );
}
