import 'package:app_indicators/src/blocs/indicators/indicators_bloc.dart';
import 'package:app_indicators/src/blocs/period/period_bloc.dart';
import 'package:app_indicators/src/blocs/worker/worker_bloc.dart';
import 'package:app_indicators/src/models/period.model.dart';
import 'package:app_indicators/src/models/period.register.request.dart';
import 'package:app_indicators/src/service/period.service.dart';
import 'package:app_indicators/src/widgets/message.empty.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

class PeriodPage extends StatelessWidget {
  const PeriodPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final periodBloc = BlocProvider.of<PeriodBloc>(context);
    final periodService = PeriodService();
    final DateFormat formatter = DateFormat('yyyy-MM-dd');

    return BlocBuilder<PeriodBloc, PeriodState>(
      builder: (context, state) {
        return RefreshIndicator(
          onRefresh: () async {
            periodBloc.add(const OnLoandingPeriodEvent(loandingPeriods: true));
            periodBloc.add(SetPeriodListEvent(periods: await periodService.getPeriods()));
            periodBloc.add(const OnLoandingPeriodEvent(loandingPeriods: false));
          },
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              children: [
                state.loandingPeriods ? const LinearProgressIndicator(minHeight: 10) : const SizedBox(),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                          child: DropdownButtonFormField(
                              hint: const Text('-- SELECCIONE PERIODO --', style: TextStyle(color: Colors.black, fontSize: 16)),
                              isExpanded: true,
                              onChanged: state.loandingPeriods
                                  ? null
                                  : (value) {
                                      Period onPeriodSelected = state.periods.where((period) => period.id == value).toList().first;
                                      periodBloc.add(OnPeriodSelectedEvent(onPeriodSelected: onPeriodSelected));
                                    },
                              items: state.periods.map((period) {
                                return DropdownMenuItem(value: period.id, child: Text("   " + period.name));
                              }).toList())),
                      const Padding(padding: EdgeInsets.all(8.0)),
                      BlocBuilder<WorkerBloc, WorkerState>(
                        builder: (context, stateWorker) {
                          return stateWorker.workers.isEmpty
                              ? const SizedBox()
                              : BlocBuilder<IndicatorsBloc, IndicatorsState>(
                                  builder: (context, stateIndicators) {
                                    return stateIndicators.indicators.isEmpty ? const SizedBox() :  IconButton(
                                      icon: const Icon(Icons.add),
                                      onPressed: state.loandingPeriods
                                          ? null
                                          : () {
                                              dialogInsertarPeriodo(context);
                                            },
                                    );
                                  },
                                );
                        },
                      )
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Visibility(visible: state.periods.isEmpty, child: const MessageEmpty(message: 'No tienes periodos registrados.')),
                if (state.onPeriodSelected != null)
                  ...state.onPeriodSelected!.periodDetails.map((periodDetail) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(horizontal: 15),
                              width: MediaQuery.of(context).size.width,
                              height: 70,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      const FaIcon(FontAwesomeIcons.calendarCheck),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          const Text('Fecha: ', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold)),
                                          Text(formatter.format(periodDetail.date)),
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          const Text('Valor Promedio', style: TextStyle(fontWeight: FontWeight.bold)),
                                          Text(
                                            periodDetail.averageByDays.toStringAsFixed(2).toString() + "%",
                                            style: TextStyle(color: periodDetail.getColor, fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      IconButton(
                                          icon: const FaIcon(FontAwesomeIcons.arrowRight, size: 20),
                                          onPressed: () async {
                                            periodBloc.add(OnPeriodDetailSelectedEvent(onPeriodDetailSelected: periodDetail));
                                            Navigator.pushNamed(context, 'detailPeriod');
                                          })
                                    ],
                                  ),
                                  const Divider(color: Colors.grey)
                                ],
                              ),
                            ),
                          ],
                        ),
                      ))
              ],
            ),
          ),
        );
      },
    );
  }

  dialogInsertarPeriodo(BuildContext context) {
    DateTime? startDate;
    DateTime? endDate;

    return showDialog(
      context: context,
      builder: (context) {
        final TextEditingController namePeriodController = TextEditingController();
        final DateFormat formatter = DateFormat('yyyy-MM-dd');

        final periodBloc = BlocProvider.of<PeriodBloc>(context);
        final periodService = PeriodService();

        return AlertDialog(
          shape: const RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
          clipBehavior: Clip.antiAliasWithSaveLayer,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text('Nuevo Periodo', style: TextStyle(fontSize: 20), maxLines: 2, overflow: TextOverflow.ellipsis),
            ],
          ),
          content: StatefulBuilder(
            builder: (context, setState) {
              return BlocBuilder<PeriodBloc, PeriodState>(
                builder: (context, state) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextFormField(
                        autofocus: true,
                        controller: namePeriodController,
                        decoration: InputDecoration(
                            hintText: "Nombre",
                            hintStyle: const TextStyle(color: Color(0xffBFBFBF)),
                            suffixIcon: IconButton(
                                icon: const Icon(Icons.calendar_today, color: Colors.lightBlue),
                                onPressed: () async {
                                  final picked = await showDateRangePicker(
                                    context: context,
                                    lastDate: DateTime(2050),
                                    firstDate: DateTime(2022),
                                  );
                                  if (picked != null) {
                                    setState(
                                      () {
                                        startDate = picked.start;
                                        endDate = picked.end;
                                      },
                                    );
                                  }
                                })),
                      ),
                      if (startDate != null && endDate != null)
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  const Text('Fecha Inicio: ', style: TextStyle(fontWeight: FontWeight.bold)),
                                  Text(startDate != null ? formatter.format(startDate!) : ''),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  const Text('Fecha Fin: ', style: TextStyle(fontWeight: FontWeight.bold)),
                                  Text(endDate != null ? formatter.format(endDate!) : ''),
                                ],
                              ),
                            ],
                          ),
                        ),
                      if (startDate == null && endDate == null) const SizedBox(height: 20),
                      ClipRRect(
                        borderRadius: const BorderRadius.all(Radius.circular(25)),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          decoration: const BoxDecoration(color: Colors.lightBlue, borderRadius: BorderRadius.all(Radius.circular(25))),
                          child: MaterialButton(
                            child: const Text('Registrar', style: TextStyle(color: Colors.white, fontSize: 16)),
                            onPressed: () async {
                              if (namePeriodController.text.trim() != '' && startDate != null && endDate != null) {
                                Navigator.pop(context);
                                periodBloc.add(const OnLoandingPeriodEvent(loandingPeriods: true));

                                PeriodRegisterRequest periodRegisterRequest = PeriodRegisterRequest(
                                  dateEnd: formatter.format(endDate!),
                                  dateInit: formatter.format(startDate!),
                                  name: namePeriodController.text.trim(),
                                );

                                await periodService.registerNewPeriod(periodRegisterRequest);

                                List<Period> periods = await periodService.getPeriods();
                                periodBloc.add(SetPeriodListEvent(periods: periods));
                                
                                periodBloc.add(const OnLoandingPeriodEvent(loandingPeriods: false));
                              }
                            },
                          ),
                        ),
                      )
                    ],
                  );
                },
              );
            },
          ),
        );
      },
    );
  }
}
