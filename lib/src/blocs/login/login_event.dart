part of 'login_bloc.dart';

abstract class LoginEvent extends Equatable {
  const LoginEvent();

  @override
  List<Object> get props => [];
}

class ChangeFlagLoandingEvent extends LoginEvent{
    const ChangeFlagLoandingEvent({ required this.loanding });
    final bool loanding;  
}

class SetUserTokenEvent extends LoginEvent {
  const SetUserTokenEvent({ required this.user });
  final User user;
}