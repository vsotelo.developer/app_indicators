part of 'login_bloc.dart';

class LoginState extends Equatable {
  const LoginState({ this.loanding = false, this.user });

  final bool loanding;
  final User? user;

  LoginState copyWith({ bool? loanding, User? user})
    => LoginState(loanding: loanding ?? this.loanding, user: user ?? this.user);

  @override
  List<Object?> get props => [ loanding, user ];
}

