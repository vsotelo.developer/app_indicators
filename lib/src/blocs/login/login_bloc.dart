import 'package:app_indicators/src/models/user.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(const LoginState()) {
    on<ChangeFlagLoandingEvent>((event, emit) => emit(state.copyWith(loanding: event.loanding)));
    on<SetUserTokenEvent>((event, emit) => emit(state.copyWith(user: event.user)));
  }
}
