part of 'dashboard_bloc.dart';

abstract class DashboardEvent extends Equatable {
  const DashboardEvent();

  @override
  List<Object> get props => [];
}

class OnChangePeriodAverageEvent extends DashboardEvent {
  const OnChangePeriodAverageEvent({required this.listPeriodsAverage});
  final List<PeriodAverage> listPeriodsAverage;
}

class OnSeLoandingAverage extends DashboardEvent {
  const OnSeLoandingAverage({required this.loandingListAverage});
  final bool loandingListAverage;
}

class OnChangeIndicatorsAverageEvent extends DashboardEvent {
  const OnChangeIndicatorsAverageEvent({required this.listAverageIndicators});
  final List<AverageIndicators> listAverageIndicators;
}

class OnSeLoandingListIndicatorsAverage extends DashboardEvent {
  const OnSeLoandingListIndicatorsAverage({required this.loandingListAverageIndicators});
  final bool loandingListAverageIndicators;
}

class OnSetPeriodNameSelected extends DashboardEvent {
   const OnSetPeriodNameSelected({ required this.namePeriodSelected});
   final String namePeriodSelected;  
}