part of 'dashboard_bloc.dart';

class DashboardState extends Equatable {
  const DashboardState({
    this.listPeriodsAverage = const [],
    this.loandingListAverage = false,
    this.loandingListAverageIndicators = false,
    this.listAverageIndicators = const [],
    this.namePeriodSelected = '',
  });

  final bool loandingListAverage;
  final bool loandingListAverageIndicators;
  final List<PeriodAverage> listPeriodsAverage;
  final List<AverageIndicators> listAverageIndicators;
  final String namePeriodSelected;

  DashboardState copyWith({
    List<PeriodAverage>? listPeriodsAverage,
    bool? loandingListAverage,
    bool? loandingListAverageIndicators,
    List<AverageIndicators>? listAverageIndicators,
    String? namePeriodSelected,
  }) =>
      DashboardState(
        listPeriodsAverage: listPeriodsAverage ?? this.listPeriodsAverage,
        loandingListAverage: loandingListAverage ?? this.loandingListAverage,
        loandingListAverageIndicators: loandingListAverageIndicators ?? this.loandingListAverageIndicators,
        listAverageIndicators: listAverageIndicators ?? this.listAverageIndicators,
        namePeriodSelected: namePeriodSelected ?? this.namePeriodSelected,
      );

  @override
  List<Object> get props => [loandingListAverage, listPeriodsAverage, loandingListAverageIndicators, listAverageIndicators, namePeriodSelected];
}
