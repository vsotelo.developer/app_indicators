import 'package:app_indicators/src/models/average.indicators.response.dart';
import 'package:app_indicators/src/models/period.average.response.dart';
import 'package:app_indicators/src/service/period.service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardEvent, DashboardState> {

  PeriodService periodService = PeriodService();

  DashboardBloc() : super(const DashboardState()) {
    on<DashboardEvent>((event, emit) {});
    on<OnChangePeriodAverageEvent>((event, emit) => emit(state.copyWith(listPeriodsAverage: event.listPeriodsAverage)));
    on<OnSeLoandingAverage>((event, emit) => emit(state.copyWith(loandingListAverage: event.loandingListAverage)));
    on<OnChangeIndicatorsAverageEvent>((event, emit) => emit(state.copyWith(listAverageIndicators: event.listAverageIndicators)));
    on<OnSeLoandingListIndicatorsAverage>((event, emit) => emit(state.copyWith(loandingListAverageIndicators: event.loandingListAverageIndicators)));
    on<OnSetPeriodNameSelected>((event, emit) => emit(state.copyWith(namePeriodSelected: event.namePeriodSelected)));
    _init();
  }

  Future<void> _init() async {
    add(const OnSeLoandingAverage(loandingListAverage: true));
    List<PeriodAverage> listPeriodsAverage = await periodService.getAveragePeriod();
    add(OnChangePeriodAverageEvent(listPeriodsAverage: listPeriodsAverage));
    add(const OnSeLoandingAverage(loandingListAverage: false));
  }
  
}

