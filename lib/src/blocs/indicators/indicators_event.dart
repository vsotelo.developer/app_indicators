part of 'indicators_bloc.dart';

abstract class IndicatorsEvent extends Equatable {
  const IndicatorsEvent();

  @override
  List<Object> get props => [];
}

class SetLoandingIndicatorsEvent extends IndicatorsEvent {
  const SetLoandingIndicatorsEvent({required this.loandingIndicators});

  final bool loandingIndicators;  
}

class SetListIndicatorsEvent extends IndicatorsEvent {
  const SetListIndicatorsEvent({ required this.indicators });
  final List<Indicator> indicators;  
}

class IsSetPorsentaje extends IndicatorsEvent {
  const IsSetPorsentaje({ required this.isSetPorcentaje });
  final bool isSetPorcentaje;  
}

class ChangeColorIndicatorEvent extends IndicatorsEvent {
  const ChangeColorIndicatorEvent({ required this.color });
  final Color color;  
}

class IsCorrectQuantityIndicators extends IndicatorsEvent {
  const IsCorrectQuantityIndicators({ required this.correctQuantityIndicators });
  final bool correctQuantityIndicators;  
}

class IsSaveIndicators extends IndicatorsEvent {
  const IsSaveIndicators({ required this.isSaveIndicators });
  final bool isSaveIndicators;  
}
