import 'package:app_indicators/src/models/indicator.model.dart';
import 'package:app_indicators/src/service/indicators.service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'indicators_event.dart';
part 'indicators_state.dart';

class IndicatorsBloc extends Bloc<IndicatorsEvent, IndicatorsState> {

  IndicatorsService indicatorsService = IndicatorsService();
  
  IndicatorsBloc() : super(const IndicatorsState(indicators: [])) {      
      on<IndicatorsEvent>((event, emit) {});
      on<SetLoandingIndicatorsEvent>((event, emit) => emit(state.copyWith(loandingIndicators: event.loandingIndicators)));
      on<SetListIndicatorsEvent>((event, emit) => emit(state.copyWith(indicators: event.indicators)));
      on<IsSetPorsentaje>((event, emit) => emit(state.copyWith(isSetPorcentaje: event.isSetPorcentaje)));
      on<ChangeColorIndicatorEvent>((event, emit) => emit(state.copyWith(color: event.color)));
      on<IsCorrectQuantityIndicators>((event, emit) => emit(state.copyWith(correctQuantityIndicators: event.correctQuantityIndicators)));
      on<IsSaveIndicators>((event, emit) => emit(state.copyWith(isSaveIndicators: event.isSaveIndicators)));
      _init();
  }

  Future<void> _init() async {
    add(const SetLoandingIndicatorsEvent(loandingIndicators: true));    
    add(SetListIndicatorsEvent(indicators: await indicatorsService.getListIndicators()));
    add(const IsCorrectQuantityIndicators(correctQuantityIndicators: true));
    add(const SetLoandingIndicatorsEvent(loandingIndicators: false));
  }

  void setListIndicators(List<Indicator> indicators) {
    
    int total = 0;
    for (var indicator in indicators) {      
      total = total + indicator.equivalentPercentage.toInt();
    }

    if (total == 100) {
      add(const IsCorrectQuantityIndicators(correctQuantityIndicators: true));
    } else {
      add(const IsCorrectQuantityIndicators(correctQuantityIndicators: false));
    }

    add(SetListIndicatorsEvent(indicators: indicators));
  }

  void saveIndicators() async {
    add(const IsSaveIndicators(isSaveIndicators: true));
    List<Indicator> indicators = await indicatorsService.saveIndicators(state.indicators);
    add(SetListIndicatorsEvent(indicators: indicators));
    add(const IsSaveIndicators(isSaveIndicators: false));
  }
  
}
