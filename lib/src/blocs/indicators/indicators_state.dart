part of 'indicators_bloc.dart';

class IndicatorsState extends Equatable {
  const IndicatorsState({
     required this.indicators,
     this.loandingIndicators = false ,
     this.isSetPorcentaje = false,
     this.isSaveIndicators = false,
     this.correctQuantityIndicators = false,
     this.color = const Color(0xff85888c) 
  });
    
  final List<Indicator> indicators;
  final bool loandingIndicators;
  final bool isSetPorcentaje;
  final bool isSaveIndicators;
  final bool correctQuantityIndicators;

  // Utils 
  final Color color;

  IndicatorsState copyWith({
    List<Indicator>? indicators,
    bool? loandingIndicators,
    bool? isSetPorcentaje,
    Color? color,
    bool? isSaveIndicators,
    bool? correctQuantityIndicators,
  })
  => IndicatorsState(
    indicators: indicators ?? this.indicators,
    loandingIndicators: loandingIndicators ?? this.loandingIndicators,
    isSetPorcentaje: isSetPorcentaje ?? this.isSetPorcentaje,
    color: color ?? this.color,
    isSaveIndicators: isSaveIndicators ?? this.isSaveIndicators,
    correctQuantityIndicators: correctQuantityIndicators ?? this.correctQuantityIndicators,
  );

  @override
  List<Object> get props => [ indicators, loandingIndicators, isSetPorcentaje, color, isSaveIndicators, correctQuantityIndicators ];
}