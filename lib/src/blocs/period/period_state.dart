part of 'period_bloc.dart';

class PeriodState extends Equatable {
  const PeriodState(
      {this.periods = const [],
      this.loandingPeriods = false,
      this.onPeriodSelected,
      this.onPeriodDetailSelected,
      this.onWorkerDetailPeriodSelected,
      this.onIndicatorValueSelected,
      this.loadingUpdateIndicators = false,
      this.listIndicatorsUpdate = const []});

  final List<Period> periods;
  final bool loandingPeriods;

  final Period? onPeriodSelected;
  final PeriodDetail? onPeriodDetailSelected;
  final WorkerDetailPeriod? onWorkerDetailPeriodSelected;
  final IndicatorValue? onIndicatorValueSelected;
  final bool loadingUpdateIndicators;
  final List<IndicatorValueUpdateRequest> listIndicatorsUpdate;

  PeriodState copyWith({
    List<Period>? periods,
    bool? loandingPeriods,
    Period? onPeriodSelected,
    PeriodDetail? onPeriodDetailSelected,
    WorkerDetailPeriod? onWorkerDetailPeriodSelected,
    IndicatorValue? onIndicatorValueSelected,
    bool? loadingUpdateIndicators,
    List<IndicatorValueUpdateRequest>? listIndicatorsUpdate,
  }) =>
      PeriodState(
        periods: periods ?? this.periods,
        loandingPeriods: loandingPeriods ?? this.loandingPeriods,
        onPeriodSelected: onPeriodSelected ?? this.onPeriodSelected,
        onPeriodDetailSelected: onPeriodDetailSelected ?? this.onPeriodDetailSelected,
        onWorkerDetailPeriodSelected: onWorkerDetailPeriodSelected ?? this.onWorkerDetailPeriodSelected,
        onIndicatorValueSelected: onIndicatorValueSelected ?? this.onIndicatorValueSelected,
        loadingUpdateIndicators: loadingUpdateIndicators ?? this.loadingUpdateIndicators,
        listIndicatorsUpdate: listIndicatorsUpdate ?? this.listIndicatorsUpdate,
      );

  PeriodState copyWithV2({
    List<Period>? periods,
    bool? loandingPeriods,
    Period? onPeriodSelected,
    PeriodDetail? onPeriodDetailSelected,
    WorkerDetailPeriod? onWorkerDetailPeriodSelected,
    IndicatorValue? onIndicatorValueSelected,
    bool? loadingUpdateIndicators,
    List<IndicatorValueUpdateRequest>? listIndicatorsUpdate,
  }) =>
      PeriodState(
        periods: const [],
        loandingPeriods: loandingPeriods ?? this.loandingPeriods,
        onPeriodSelected: onPeriodSelected ?? this.onPeriodSelected,
        onPeriodDetailSelected: onPeriodDetailSelected ?? this.onPeriodDetailSelected,
        onWorkerDetailPeriodSelected: onWorkerDetailPeriodSelected ?? this.onWorkerDetailPeriodSelected,
        onIndicatorValueSelected: onIndicatorValueSelected ?? this.onIndicatorValueSelected,
        loadingUpdateIndicators: loadingUpdateIndicators ?? this.loadingUpdateIndicators,
        listIndicatorsUpdate: listIndicatorsUpdate ?? this.listIndicatorsUpdate,
      );      

  @override
  List<Object?> get props => [
        periods,
        loandingPeriods,
        onPeriodSelected,
        onPeriodDetailSelected,
        onWorkerDetailPeriodSelected,
        onIndicatorValueSelected,
        loadingUpdateIndicators,
        listIndicatorsUpdate
      ];
}
