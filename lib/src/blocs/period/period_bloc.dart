import 'package:app_indicators/src/models/indicator.value.model.dart';
import 'package:app_indicators/src/models/period.detail.dart';
import 'package:app_indicators/src/models/period.model.dart';
import 'package:app_indicators/src/models/update.indicator.request.dart';
import 'package:app_indicators/src/models/worker.detail.period.model.dart';
import 'package:app_indicators/src/service/period.service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'period_event.dart';
part 'period_state.dart';

class PeriodBloc extends Bloc<PeriodEvent, PeriodState> {

  PeriodService periodService = PeriodService();

  PeriodBloc() : super(const PeriodState(periods: [])) {
    on<PeriodEvent>((event, emit) { });
    on<SetPeriodListEvent>((event, emit) => emit(state.copyWith(periods: event.periods)));
    on<OnLoandingPeriodEvent>((event, emit) => emit(state.copyWith(loandingPeriods: event.loandingPeriods)));
    on<OnPeriodSelectedEvent>((event, emit) => emit(state.copyWith(onPeriodSelected: event.onPeriodSelected)));
    on<OnPeriodDetailSelectedEvent>((event, emit) => emit(state.copyWith(onPeriodDetailSelected: event.onPeriodDetailSelected)));
    on<OnWorkerDetailPeriodSelectedEvent>((event, emit) => emit(state.copyWith(onWorkerDetailPeriodSelected: event.onWorkerDetailPeriodSelected)));
    on<OnIndicatorValueSelectedEvent>((event, emit) => emit(state.copyWith(onIndicatorValueSelected: event.onIndicatorValueSelected)));
    on<OnLoadingUpdateIndicators>((event, emit) => emit(state.copyWith(loadingUpdateIndicators: event.loadingUpdateIndicators)));
    
    on<OnAddIndicatorValueUpdate>((event, emit) {

      List<IndicatorValueUpdateRequest> listIndicatorsUpdate = state.listIndicatorsUpdate;
      List<IndicatorValueUpdateRequest> clonelistIndicatorsUpdate = [];

      for (var element in listIndicatorsUpdate) {
        clonelistIndicatorsUpdate.add(element);
      }
      
      bool isNotexistToUpdate = true;

      for (var itemToUpdate in clonelistIndicatorsUpdate) {
        if (itemToUpdate.indicatorValueId == event.indicatorValueUpdateRequest.indicatorValueId 
              && itemToUpdate.value == event.indicatorValueUpdateRequest.value) {
          isNotexistToUpdate = false;
          break;
        }
      }

      if (isNotexistToUpdate) {
        clonelistIndicatorsUpdate.add(event.indicatorValueUpdateRequest);
      }

      emit(state.copyWith(listIndicatorsUpdate : clonelistIndicatorsUpdate));
    });       

    
    on<OnCleanIndicatorsValueUpdate>((event, emit) => emit(state.copyWith(listIndicatorsUpdate: [])));    
    _init();

  }

  void _init() async {
    add(const OnLoandingPeriodEvent(loandingPeriods: true));
    add(SetPeriodListEvent(periods: await periodService.getPeriods()));
    add(const OnLoandingPeriodEvent(loandingPeriods: false));
  }
}
