part of 'period_bloc.dart';

abstract class PeriodEvent extends Equatable {
  const PeriodEvent();

  @override
  List<Object> get props => [];
}

class SetPeriodListEvent extends PeriodEvent {
  const SetPeriodListEvent({ required this.periods});
  final List<Period> periods;  
}

class OnLoandingPeriodEvent extends PeriodEvent {
  const OnLoandingPeriodEvent({ required this.loandingPeriods});
  final bool loandingPeriods;  
}

class OnPeriodSelectedEvent extends PeriodEvent {
  const OnPeriodSelectedEvent({ required this.onPeriodSelected });
  final Period onPeriodSelected;  
}

class OnPeriodDetailSelectedEvent extends PeriodEvent {
  const OnPeriodDetailSelectedEvent({ required this.onPeriodDetailSelected });
  final PeriodDetail onPeriodDetailSelected;  
}

class OnWorkerDetailPeriodSelectedEvent extends PeriodEvent {
  const OnWorkerDetailPeriodSelectedEvent({ required this.onWorkerDetailPeriodSelected });
  final WorkerDetailPeriod onWorkerDetailPeriodSelected;  
}

class OnIndicatorValueSelectedEvent extends PeriodEvent {
  const OnIndicatorValueSelectedEvent({ required this.onIndicatorValueSelected });
  final IndicatorValue onIndicatorValueSelected;
}

class OnLoadingUpdateIndicators extends PeriodEvent {
  const OnLoadingUpdateIndicators({ required this.loadingUpdateIndicators});
  final bool loadingUpdateIndicators;  
}

class OnAddIndicatorValueUpdate extends PeriodEvent {
  const OnAddIndicatorValueUpdate({ required this.indicatorValueUpdateRequest });
  final IndicatorValueUpdateRequest indicatorValueUpdateRequest;
}

class OnCleanIndicatorsValueUpdate extends PeriodEvent {}