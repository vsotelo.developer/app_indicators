import 'package:app_indicators/src/models/worker.model.dart';
import 'package:app_indicators/src/service/workers.service.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'worker_event.dart';
part 'worker_state.dart';

class WorkerBloc extends Bloc<WorkerEvent, WorkerState> {

  WorkerService workerService = WorkerService();

  WorkerBloc() : super(const WorkerState(workers: [])) {
    on<WorkerEvent>((event, emit) {});
    on<SetWorkersListEvent>((event, emit) => emit(state.copyWith(workers: event.workers)));
    on<OnLoandingWorkersEvent>((event, emit) => emit(state.copyWith(loandingWorkers: event.loandingWorkers)));
    on<OnChangeLoandingUploadEvent>((event, emit) => emit(state.copyWith(loandingUpload: event.loandingUpload)));
    on<OnChangeLoandingDownloadEvent>((event, emit) => emit(state.copyWith(loandingDowload: event.loandingDowload)));
    _init();
  }

  void _init() async {
    add(const OnLoandingWorkersEvent(loandingWorkers: true));    
    add(SetWorkersListEvent(workers: await workerService.getWorkers()));    
    add(const OnLoandingWorkersEvent(loandingWorkers: false));
  }

}
