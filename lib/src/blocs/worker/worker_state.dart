part of 'worker_bloc.dart';

class WorkerState extends Equatable {
  const WorkerState({
    required this.workers,
    this.loandingWorkers = false,
    this.loandingUpload = false,
    this.loandingDowload = false,
  });

  final List<Worker> workers;
  final bool loandingWorkers;
  final bool loandingUpload;
  final bool loandingDowload;

  WorkerState copyWith({
    List<Worker>? workers,
    bool? loandingWorkers,
    bool? loandingUpload,
    bool? loandingDowload,
  }) =>
      WorkerState(
        workers: workers ?? this.workers,
        loandingWorkers: loandingWorkers ?? this.loandingWorkers,
        loandingUpload: loandingUpload ?? this.loandingUpload,
        loandingDowload: loandingDowload ?? this.loandingDowload,
      );

  @override
  List<Object> get props => [workers, loandingWorkers, loandingDowload, loandingUpload];
}
