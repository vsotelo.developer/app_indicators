part of 'worker_bloc.dart';

abstract class WorkerEvent extends Equatable {
  const WorkerEvent();

  @override
  List<Object> get props => [];
}

class SetWorkersListEvent extends WorkerEvent {
  const SetWorkersListEvent({ required this.workers});
  final List<Worker> workers;  
}

class OnLoandingWorkersEvent extends WorkerEvent {
  const OnLoandingWorkersEvent({ required this.loandingWorkers});
  final bool loandingWorkers;  
}

class OnChangeLoandingUploadEvent extends WorkerEvent {
  const OnChangeLoandingUploadEvent({ required this.loandingUpload});
  final bool loandingUpload;  
}

class OnChangeLoandingDownloadEvent extends WorkerEvent {
  const OnChangeLoandingDownloadEvent({ required this.loandingDowload});
  final bool loandingDowload;  
}