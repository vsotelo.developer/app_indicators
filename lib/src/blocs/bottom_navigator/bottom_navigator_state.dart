part of 'bottom_navigator_bloc.dart';

class BottomNavigatorState extends Equatable {
  const BottomNavigatorState({ this.currentIndex = 0 });

  final int currentIndex;

  BottomNavigatorState copyWith({ int? currentIndex})
    => BottomNavigatorState(currentIndex: currentIndex ?? this.currentIndex);
  
  @override
  List<Object> get props => [ currentIndex ];
}

