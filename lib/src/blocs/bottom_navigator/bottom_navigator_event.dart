part of 'bottom_navigator_bloc.dart';

abstract class BottomNavigatorEvent extends Equatable {
  const BottomNavigatorEvent();

  @override
  List<Object> get props => [];
}

class ChangeCurrentIndexEvent extends BottomNavigatorEvent {
  const ChangeCurrentIndexEvent({ required this.currentIndex });
  final int currentIndex;
}