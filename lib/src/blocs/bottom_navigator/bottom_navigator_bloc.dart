import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'bottom_navigator_event.dart';
part 'bottom_navigator_state.dart';

class BottomNavigatorBloc extends Bloc<BottomNavigatorEvent, BottomNavigatorState> {
  BottomNavigatorBloc() : super(const BottomNavigatorState()) {
    on<ChangeCurrentIndexEvent>((event, emit) => emit(state.copyWith(currentIndex: event.currentIndex)));
  }
}
