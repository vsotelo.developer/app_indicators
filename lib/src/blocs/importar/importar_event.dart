part of 'importar_bloc.dart';

abstract class ImportarEvent extends Equatable {
  const ImportarEvent();

  @override
  List<Object> get props => [];
}
