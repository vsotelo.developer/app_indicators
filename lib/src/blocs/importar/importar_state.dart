part of 'importar_bloc.dart';

abstract class ImportarState extends Equatable {
  const ImportarState();
  
  @override
  List<Object> get props => [];
}

class ImportarInitial extends ImportarState {}
