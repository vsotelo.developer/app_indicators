import 'dart:convert';

class User {
    User({
        required this.documentNumber,
        required this.globalId,
        required this.exp,
        required this.iat,
    });

    final String documentNumber;
    final String globalId;
    final int exp;
    final int iat;

    factory User.fromJson(String str) => User.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory User.fromMap(Map<String, dynamic> json) => User(
        documentNumber: json["documentNumber"],
        globalId: json["globalId"],
        exp: json["exp"],
        iat: json["iat"],
    );

    Map<String, dynamic> toMap() => {
        "documentNumber": documentNumber,
        "globalId": globalId,
        "exp": exp,
        "iat": iat,
    };
}
