import 'dart:convert';

import 'package:app_indicators/src/models/worker.detail.period.model.dart';
import 'package:flutter/material.dart';

class PeriodDetail {
    PeriodDetail({
        required this.averageByDays,
        required this.date,
        required this.id,
        required this.workerDetailPeriods,
    });

    final double averageByDays;
    final DateTime date;
    final String id;
    final List<WorkerDetailPeriod> workerDetailPeriods;

    factory PeriodDetail.fromJson(String str) => PeriodDetail.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PeriodDetail.fromMap(Map<String, dynamic> json) => PeriodDetail(
        averageByDays: json["averageByDays"],
        date: DateTime.parse(json["date"]),
        id: json["id"],
        workerDetailPeriods: List<WorkerDetailPeriod>.from(json["workerDetailPeriods"].map((x) => WorkerDetailPeriod.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "averageByDays": averageByDays,
        "date": date.toIso8601String(),
        "id": id,
        "workerDetailPeriods": List<dynamic>.from(workerDetailPeriods.map((x) => x.toMap())),
    };

    Color get getColor {
      if(averageByDays < 50 ){
         return Colors.red;
      }
      return Colors.green;
    }
}

