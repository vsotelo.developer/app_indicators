import 'dart:convert';

import 'package:app_indicators/src/models/indicator.model.dart';

class AverageIndicatorsResponse {
    AverageIndicatorsResponse({
        required this.message,
        required this.result,
    });

    final String message;
    final List<AverageIndicators> result;

    factory AverageIndicatorsResponse.fromJson(String str) => AverageIndicatorsResponse.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory AverageIndicatorsResponse.fromMap(Map<String, dynamic> json) => AverageIndicatorsResponse(
        message: json["message"],
        result: List<AverageIndicators>.from(json["result"].map((x) => AverageIndicators.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toMap())),
    };
}

class AverageIndicators {
    AverageIndicators({
        required this.index,
        required this.indicator,
        required this.average,
    });

    final int index;
    final Indicator indicator;
    final double average;

    factory AverageIndicators.fromJson(String str) => AverageIndicators.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory AverageIndicators.fromMap(Map<String, dynamic> json) => AverageIndicators(
        index: json["index"],
        indicator: Indicator.fromMap(json["indicator"]),
        average: json["average"],
    );

    Map<String, dynamic> toMap() => {
        "index": index,
        "indicator": indicator.toMap(),
        "average": average,
    };
}
