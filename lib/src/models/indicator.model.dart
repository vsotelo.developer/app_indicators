import 'dart:convert';

class Indicator {
    Indicator({
        required this.equivalentPercentage,
        required this.hexColor,
        required this.id,
        required this.name,
    });

    final double equivalentPercentage;
    final String hexColor;
    final String id;
    final String name;

    factory Indicator.fromJson(String str) => Indicator.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Indicator.fromMap(Map<String, dynamic> json) => Indicator(
        equivalentPercentage: json["equivalentPercentage"],
        hexColor: json["hexColor"],
        id: json["id"],
        name: json["name"],
    );

    Map<String, dynamic> toMap() => {
        "equivalentPercentage": equivalentPercentage,
        "hexColor": hexColor,
        "id": id,
        "name": name,
    };
}
