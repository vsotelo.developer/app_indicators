import 'dart:convert';

import 'indicator.model.dart';

class IndicatorValue {
    IndicatorValue({
        required this.id,
        required this.indicator,
        required this.valorIndicatorEquivalent,
        required this.value,
    });

    final String id;
    final Indicator indicator;
    final double valorIndicatorEquivalent;
    final double value;

    factory IndicatorValue.fromJson(String str) => IndicatorValue.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory IndicatorValue.fromMap(Map<String, dynamic> json) => IndicatorValue(
        id: json["id"],
        indicator: Indicator.fromMap(json["indicator"]),
        valorIndicatorEquivalent: json["valorIndicatorEquivalent"],
        value: json["value"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "indicator": indicator.toMap(),
        "valorIndicatorEquivalent": valorIndicatorEquivalent,
        "value": value,
    };

    double? _newValue;
    double? get getNewValue => _newValue;
    set setNewValue(double newValue) { _newValue = newValue; }

}