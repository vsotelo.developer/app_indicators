import 'dart:convert';

class PeriodRegisterRequest {
    PeriodRegisterRequest({
        required this.dateEnd,
        required this.dateInit,
        required this.name,
    });

    final String dateEnd;
    final String dateInit;
    final String name;

    factory PeriodRegisterRequest.fromJson(String str) => PeriodRegisterRequest.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PeriodRegisterRequest.fromMap(Map<String, dynamic> json) => PeriodRegisterRequest(
        dateEnd: json["dateEnd"],
        dateInit: json["dateInit"],
        name: json["name"],
    );

    Map<String, dynamic> toMap() => {
        "dateEnd": dateEnd,
        "dateInit": dateInit,
        "name": name,
    };
}
