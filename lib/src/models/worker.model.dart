import 'dart:convert';

class Worker {
    Worker({
        required this.id,
        required this.documentNumber,
        required this.name,
        required this.email,
    });

    final String id;
    final String documentNumber;
    final String name;
    final String email;

    factory Worker.fromJson(String str) => Worker.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Worker.fromMap(Map<String, dynamic> json) => Worker(
        id: json["id"],
        documentNumber: json["documentNumber"],
        name: json["name"],
        email: json["email"],
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "documentNumber": documentNumber,
        "name": name,
        "email": email,
    };
}
