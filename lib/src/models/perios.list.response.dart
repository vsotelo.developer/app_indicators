import 'dart:convert';

import 'package:app_indicators/src/models/period.model.dart';

class PeriodListResponse {
    PeriodListResponse({
        required this.message,
        required this.result,
    });

    final String message;
    final List<Period> result;

    factory PeriodListResponse.fromJson(String str) => PeriodListResponse.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PeriodListResponse.fromMap(Map<String, dynamic> json) => PeriodListResponse(
        message: json["message"],
        result: List<Period>.from(json["result"].map((x) => Period.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toMap())),
    };
}



