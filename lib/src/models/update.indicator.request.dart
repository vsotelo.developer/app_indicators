import 'dart:convert';

class IndicatorValueUpdateRequest {
    IndicatorValueUpdateRequest({
        required this.indicatorValueId,
        required this.periodDetailId,
        required this.periodId,
        required this.value,
        required this.workerPeriodDetailId,
    });

    final String indicatorValueId;
    final String periodDetailId;
    final String periodId;
    final double value;
    final String workerPeriodDetailId;

    factory IndicatorValueUpdateRequest.fromJson(String str) => IndicatorValueUpdateRequest.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory IndicatorValueUpdateRequest.fromMap(Map<String, dynamic> json) => IndicatorValueUpdateRequest(
        indicatorValueId: json["indicatorValueId"],
        periodDetailId: json["periodDetailId"],
        periodId: json["periodId"],
        value: json["value"].toDouble(),
        workerPeriodDetailId: json["workerPeriodDetailId"],
    );

    Map<String, dynamic> toMap() => {
        "indicatorValueId": indicatorValueId,
        "periodDetailId": periodDetailId,
        "periodId": periodId,
        "value": value,
        "workerPeriodDetailId": workerPeriodDetailId,
    };
}
