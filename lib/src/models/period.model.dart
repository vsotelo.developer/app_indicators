import 'dart:convert';

import 'package:app_indicators/src/models/period.detail.dart';

class Period {
    Period({
        required this.averageByDays,
        required this.dateEnd,
        required this.dateInit,
        required this.id,
        required this.name,
        required this.periodDetails,
    });

    final double averageByDays;
    final DateTime dateEnd;
    final DateTime dateInit;
    final String id;
    final String name;
    final List<PeriodDetail> periodDetails;

    factory Period.fromJson(String str) => Period.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Period.fromMap(Map<String, dynamic> json) => Period(
        averageByDays: json["averageByDays"],
        dateEnd: DateTime.parse(json["dateEnd"]),
        dateInit: DateTime.parse(json["dateInit"]),
        id: json["id"],
        name: json["name"],
        periodDetails: List<PeriodDetail>.from(json["periodDetails"].map((x) => PeriodDetail.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "averageByDays": averageByDays,
        "dateEnd": dateEnd.toIso8601String(),
        "dateInit": dateInit.toIso8601String(),
        "id": id,
        "name": name,
        "periodDetails": List<dynamic>.from(periodDetails.map((x) => x.toMap())),
    };
}



