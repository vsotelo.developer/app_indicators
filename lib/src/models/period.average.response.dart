// To parse this JSON data, do
//
//     final periodAverageResponse = periodAverageResponseFromMap(jsonString);

import 'dart:convert';

class PeriodAverageResponse {
    PeriodAverageResponse({
        required this.message,
        required this.result,
    });

    final String message;
    final List<PeriodAverage> result;

    factory PeriodAverageResponse.fromJson(String str) => PeriodAverageResponse.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PeriodAverageResponse.fromMap(Map<String, dynamic> json) => PeriodAverageResponse(
        message: json["message"],
        result: List<PeriodAverage>.from(json["result"].map((x) => PeriodAverage.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toMap())),
    };
}

class PeriodAverage {
    PeriodAverage({
        required this.periodId,
        required this.index,
        required this.value,
        required this.periodName,
    });

    final String periodId;
    final int index;
    final double value;
    final String periodName;

    factory PeriodAverage.fromJson(String str) => PeriodAverage.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PeriodAverage.fromMap(Map<String, dynamic> json) => PeriodAverage(
        periodId: json["periodId"],
        index: json["index"],
        value: json["value"],
        periodName: json["periodName"],
    );

    Map<String, dynamic> toMap() => {
        "periodId": periodId,
        "index": index,
        "value": value,
        "periodName": periodName,
    };
}
