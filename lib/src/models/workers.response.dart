import 'dart:convert';

import 'package:app_indicators/src/models/worker.model.dart';

class WorkerResponse {
    WorkerResponse({
        required this.message,
        required this.result,
    });

    final String message;
    final List<Worker> result;

    factory WorkerResponse.fromJson(String str) => WorkerResponse.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory WorkerResponse.fromMap(Map<String, dynamic> json) => WorkerResponse(
        message: json["message"],
        result: List<Worker>.from(json["result"].map((x) => Worker.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toMap())),
    };
}

