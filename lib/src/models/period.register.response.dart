import 'dart:convert';

import 'package:app_indicators/src/models/period.model.dart';

class PeriodRegisterResponse {
    PeriodRegisterResponse({
        required this.message,
        required this.result,
    });

    final String message;
    final Period result;

    factory PeriodRegisterResponse.fromJson(String str) => PeriodRegisterResponse.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory PeriodRegisterResponse.fromMap(Map<String, dynamic> json) => PeriodRegisterResponse(
        message: json["message"],
        result: Period.fromMap(json["result"]),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": result.toMap(),
    };
}