import 'dart:convert';

class LoginResponse {
    LoginResponse({
        required this.message,
        required this.result,
    });

    final String message;
    final Result result;

    factory LoginResponse.fromJson(String str) => LoginResponse.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory LoginResponse.fromMap(Map<String, dynamic> json) => LoginResponse(
        message: json["message"],
        result: Result.fromMap(json["result"]),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": result.toMap(),
    };
}

class Result {
    Result({
        required this.token,
    });

    final String token;

    factory Result.fromJson(String str) => Result.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Result.fromMap(Map<String, dynamic> json) => Result(
        token: json["token"],
    );

    Map<String, dynamic> toMap() => {
        "token": token,
    };
}
