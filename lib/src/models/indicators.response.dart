import 'dart:convert';

import 'package:app_indicators/src/models/indicator.model.dart';

class UserIndicatorsResponse {
    UserIndicatorsResponse({
        required this.message,
        required this.result,
    });

    final String message;
    final List<Indicator> result;

    factory UserIndicatorsResponse.fromJson(String str) => UserIndicatorsResponse.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory UserIndicatorsResponse.fromMap(Map<String, dynamic> json) => UserIndicatorsResponse(
        message: json["message"],
        result: List<Indicator>.from(json["result"].map((x) => Indicator.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toMap())),
    };
}

