import 'dart:convert';

import 'package:app_indicators/src/models/indicator.value.model.dart';
import 'package:app_indicators/src/models/worker.model.dart';

class WorkerDetailPeriod {
    WorkerDetailPeriod({
        required this.id,
        required this.indicatorValues,
        required this.valueAverage,
        required this.worker,
    });

    final String id;
    final List<IndicatorValue> indicatorValues;
    final double valueAverage;
    final Worker worker;

    factory WorkerDetailPeriod.fromJson(String str) => WorkerDetailPeriod.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory WorkerDetailPeriod.fromMap(Map<String, dynamic> json) => WorkerDetailPeriod(
        id: json["id"],
        indicatorValues: List<IndicatorValue>.from(json["indicatorValues"].map((x) => IndicatorValue.fromMap(x))),
        valueAverage: json["valueAverage"],
        worker: Worker.fromMap(json["worker"]),
    );

    Map<String, dynamic> toMap() => {
        "id": id,
        "indicatorValues": List<dynamic>.from(indicatorValues.map((x) => x.toMap())),
        "valueAverage": valueAverage,
        "worker": worker.toMap(),
    };

    bool _selected = false;

    bool get getSelected => _selected;
    set setSelected(bool selected) { _selected = selected; }    
}

