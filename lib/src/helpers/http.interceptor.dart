import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class HttpInterceptor extends Interceptor {

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {    
    const storage = FlutterSecureStorage();
    String? token = await storage.read(key: 'token');
    options.headers['Authorization'] = token;
    super.onRequest(options, handler);
  }
}