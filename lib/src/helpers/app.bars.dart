import 'dart:io';

import 'package:app_indicators/src/blocs/indicators/indicators_bloc.dart';
import 'package:app_indicators/src/blocs/period/period_bloc.dart';
import 'package:app_indicators/src/blocs/worker/worker_bloc.dart';
import 'package:app_indicators/src/models/period.detail.dart';
import 'package:app_indicators/src/models/period.model.dart';
import 'package:app_indicators/src/service/period.service.dart';
import 'package:app_indicators/src/service/workers.service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:badges/badges.dart';

AppBar buildAppBarDashboard(BuildContext context) {
  return AppBar(
    toolbarHeight: 100,
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text('Dashboard', style: TextStyle(fontSize: 25)),
        SizedBox(height: 4),
        Text('Monitorea el rendimiento', style: TextStyle(fontSize: 13.5)),
      ],
    ),
    actions: [IconButton(onPressed: () {}, icon: const Icon(Icons.search_rounded, size: 30))],
  );
}

AppBar buildAppBarIndicators(BuildContext context) {
  final indicatorsBloc = BlocProvider.of<IndicatorsBloc>(context);
  return AppBar(
    toolbarHeight: 100,
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text('Indicadores', style: TextStyle(fontSize: 25)),
        SizedBox(height: 4),
        Text('Parametriza los indicadores', style: TextStyle(fontSize: 13.5)),
      ],
    ),
    actions: [
      BlocBuilder<IndicatorsBloc, IndicatorsState>(
        builder: (context, state) {
          return state.isSaveIndicators
              ? const _CircularProgressWidget()
              : IconButton(
                  icon: const Icon(Icons.save, size: 30),
                  onPressed: !state.correctQuantityIndicators
                      ? null
                      : () async {
                          indicatorsBloc.saveIndicators();
                        });
        },
      ),
    ],
  );
}

AppBar buildAppBarWorkers(BuildContext context) {
  WorkerService workerService = WorkerService();
  WorkerBloc workerBloc = BlocProvider.of<WorkerBloc>(context);
  return AppBar(
    toolbarHeight: 100,
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text('Colaboradores', style: TextStyle(fontSize: 25)),
        SizedBox(height: 4),
        Text('Importa el registro de tus colaboradores', style: TextStyle(fontSize: 13.5)),
      ],
    ),
    actions: [
      BlocBuilder<WorkerBloc, WorkerState>(
        builder: (context, state) {
          return Column(
            children: [
              IconButton(
                icon: const Icon(Icons.cloud_download, size: 30),
                onPressed: state.loandingDowload ? null :() async {
                  workerBloc.add(const OnChangeLoandingDownloadEvent(loandingDowload: true));
                  await workerService.downloadFile();
                  workerBloc.add(const OnChangeLoandingDownloadEvent(loandingDowload: false));
                }
              ),
              IconButton(
                icon: const Icon(Icons.cloud_upload, size: 30),
                onPressed: state.loandingUpload ? null :() async {
                  workerBloc.add(const OnChangeLoandingUploadEvent(loandingUpload: true));
                  await workerService.selectFile();
                                    
                  workerBloc.add(const OnLoandingWorkersEvent(loandingWorkers: true));    
                  workerBloc.add(SetWorkersListEvent(workers: await workerService.getWorkers()));    
                  workerBloc.add(const OnLoandingWorkersEvent(loandingWorkers: false));

                  workerBloc.add(const OnChangeLoandingUploadEvent(loandingUpload: false));
                }
              ),
            ],
          );
        },
      ),
      const SizedBox(width: 10)
    ],
  );
}

AppBar buildAppBarImport(BuildContext context) {
  return AppBar(
    toolbarHeight: 100,
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text('Periodo', style: TextStyle(fontSize: 25)),
        SizedBox(height: 4),
        Text('Administra los indicadores por periodo', style: TextStyle(fontSize: 13.5)),
      ],
    ),
  );
}

AppBar buildAppBarDetailPeriod(BuildContext context) {
  final periodBloc = BlocProvider.of<PeriodBloc>(context);
  final periodService = PeriodService();
  return AppBar(
    toolbarHeight: 100,
    title: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text('Detalle', style: TextStyle(fontSize: 25)),
        SizedBox(height: 4),
        Text('Consulta el detalle de tu colaborador', style: TextStyle(fontSize: 13.5)),
      ],
    ),
    actions: [
      BlocBuilder<PeriodBloc, PeriodState>(
        builder: (context, state) {
          return state.loadingUpdateIndicators
              ? const _CircularProgressWidget()
              : Transform.translate(
                  offset: const Offset(-25, 0),
                  child: GestureDetector(
                    child: Center(
                      child: Badge(
                          badgeContent: Text(state.listIndicatorsUpdate.length.toString(), style: const TextStyle(fontSize: 20, color: Colors.white)),
                          child: const Icon(Icons.save_as, size: 50)),
                    ),
                    onTap: state.listIndicatorsUpdate.isEmpty
                        ? null
                        : () async {
                            periodBloc.add(const OnLoadingUpdateIndicators(loadingUpdateIndicators: true));
                            PeriodService service = PeriodService();
                            await service.updateMasiveIndicators(state.listIndicatorsUpdate);
                            periodBloc.add(OnCleanIndicatorsValueUpdate());

                            String onPeriodSelectedActualId = state.onPeriodSelected!.id;
                            String onPeriodDetailSelectedActualId = state.onPeriodDetailSelected!.id;

                            // Reiniciar todo!
                            List<Period> periods = await periodService.getPeriods();
                            periodBloc.add(SetPeriodListEvent(periods: periods));
                            Period period = periods.where((element) => element.id == onPeriodSelectedActualId).first;
                            PeriodDetail periodDetail = period.periodDetails.where((element) => element.id == onPeriodDetailSelectedActualId).first;
                            periodBloc.add(OnPeriodSelectedEvent(onPeriodSelected: period));
                            periodBloc.add(OnPeriodDetailSelectedEvent(onPeriodDetailSelected: periodDetail));
                            periodBloc.add(const OnLoadingUpdateIndicators(loadingUpdateIndicators: false));
                          },
                  ),
                );
        },
      ),
    ],
  );
}

/*
IconButton(
              icon: const Icon(Icons.save, size: 40),
              onPressed: PeriodService.listIndicatorsUpdate.isEmpty ? null : () async {
                periodBloc.add(const OnLoadingUpdateIndicators(loadingUpdateIndicators: true));
                PeriodService service = PeriodService();
                await service.updateMasiveIndicators();
                periodBloc.add(const OnLoadingUpdateIndicators(loadingUpdateIndicators: false));
              },
            )
*/
class _CircularProgressWidget extends StatelessWidget {
  const _CircularProgressWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(right: 15),
      child: Center(child: CircularProgressIndicator(color: Colors.white)),
    );
  }
}
