import 'package:app_indicators/src/blocs/dashboard/dashboard_bloc.dart';
import 'package:app_indicators/src/blocs/indicators/indicators_bloc.dart';
import 'package:app_indicators/src/blocs/login/login_bloc.dart';
import 'package:app_indicators/src/blocs/period/period_bloc.dart';
import 'package:app_indicators/src/blocs/worker/worker_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sizer/sizer.dart';

class NavigatiomDrawer extends StatelessWidget {
  const NavigatiomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const storage = FlutterSecureStorage();    
    return SafeArea(
      child: ClipRRect(
        borderRadius: const BorderRadius.only(topRight: Radius.circular(25), bottomRight: Radius.circular(25)),
        child: Drawer(
          child: Container(
            margin: EdgeInsets.only(top: 10.h),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const _HeaderNavigator(),
                Expanded(
                    child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 6.w, vertical: 8.h),
                )),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ClipRRect(
                    borderRadius: const BorderRadius.all(Radius.circular(20)),
                    child: Container(
                      width: double.infinity,
                      height: 40,
                      decoration: const BoxDecoration(color: Colors.red, borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: MaterialButton(
                          child: const Text('Cerrar Sesión y Salir', style: TextStyle(color: Colors.white, fontSize: 15)),
                          onPressed: () async {
                            await storage.delete(key: 'token');

                            /*WorkerBloc workerBloc = BlocProvider.of<WorkerBloc>(context);
                            PeriodBloc periodBloc = BlocProvider.of<PeriodBloc>(context);
                            DashboardBloc dashboardBloc = BlocProvider.of<DashboardBloc>(context);
                            IndicatorsBloc indicatorsBloc = BlocProvider.of<IndicatorsBloc>(context);

                            workerBloc.add(const SetWorkersListEvent(workers: []));

                            periodBloc.add(const SetPeriodListEvent(periods: []));

                            indicatorsBloc.add(const SetListIndicatorsEvent(indicators: []));

                            dashboardBloc.add(const OnChangePeriodAverageEvent(listPeriodsAverage: []));
                            dashboardBloc.add(const OnChangeIndicatorsAverageEvent(listAverageIndicators: []));
                            dashboardBloc.add(const OnSetPeriodNameSelected(namePeriodSelected: ''));
                            */
                            //Navigator.pushReplacementNamed(context, 'login');
                            SystemNavigator.pop();
                          }),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _HeaderNavigator extends StatelessWidget {
  const _HeaderNavigator({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool isDarkMode = SchedulerBinding.instance!.window.platformBrightness == Brightness.dark;
    Color color = isDarkMode ? Colors.white : Colors.black;
    return Padding(
      padding: const EdgeInsets.only(left: 15, top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.height * 0.1,
            height: 80,
            child: const CircleAvatar(
              child: Text('USR', style: TextStyle(fontSize: 30)),
            ),
          ),
          Container(
            padding: const EdgeInsets.only(left: 20),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                'Hola,',
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold, color: color),
              ),
              BlocBuilder<LoginBloc, LoginState>(
                builder: (context, state) {
                  return Text(
                    state.user!.documentNumber,
                    maxLines: 2,
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500, color: color),
                  );
                },
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
