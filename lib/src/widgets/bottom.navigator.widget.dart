import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../blocs/bottom_navigator/bottom_navigator_bloc.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  const CustomBottomNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bottomNavigatorBloc = BlocProvider.of<BottomNavigatorBloc>(context);
    return BlocBuilder<BottomNavigatorBloc, BottomNavigatorState>(
      builder: (context, state) {
        return BottomNavigationBar(
          onTap: (value) => bottomNavigatorBloc.add(ChangeCurrentIndexEvent(currentIndex: value)),
          currentIndex: state.currentIndex,
          elevation: 10,          
          type: BottomNavigationBarType.fixed,
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(icon: Icon(Icons.dashboard, size: 20,), label: 'Dashboard', tooltip: ''),
            BottomNavigationBarItem(icon: FaIcon(FontAwesomeIcons.chartPie, size: 20), label: 'Indicadores', tooltip: ''),
            BottomNavigationBarItem(icon: FaIcon(FontAwesomeIcons.userGroup, size: 20), label: 'Colaboradores', tooltip: ''),
            BottomNavigationBarItem(icon: FaIcon(FontAwesomeIcons.calendar, size: 20), label: 'Periodos', tooltip: ''),
          ],
        );
      },
    );
  }
}