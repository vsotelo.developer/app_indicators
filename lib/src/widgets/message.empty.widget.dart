import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MessageEmpty extends StatelessWidget {
  const MessageEmpty({ Key? key, required this.message }) : super(key: key);

  final String message;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 250),
      child: Center(child: Column(
        children: [
          const FaIcon(FontAwesomeIcons.faceSadCry, color: Colors.grey, size: 50),
          const SizedBox(height: 10),
          Text(message, style: const TextStyle(color: Colors.grey)),
        ],
      )),
    );
  }
}
