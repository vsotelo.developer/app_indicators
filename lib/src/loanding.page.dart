import 'package:app_indicators/src/blocs/login/login_bloc.dart';
import 'package:app_indicators/src/login.page.dart';
import 'package:app_indicators/src/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:jwt_decode/jwt_decode.dart';

import 'home.page.dart';

class LoandingScreen extends StatelessWidget {
   
  const LoandingScreen({Key? key}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    
    final loginBloc = BlocProvider.of<LoginBloc>(context);
    const storage = FlutterSecureStorage();
    Future<String?> futureToken = storage.read(key: 'token');

    return Scaffold(
      body: FutureBuilder<String?>(
        future: futureToken,        
        builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
            if(snapshot.hasData){
               if(snapshot.data != null) {
                  Map<String, dynamic> payload = Jwt.parseJwt(snapshot.data!);
                  User user = User.fromMap(payload);
                  loginBloc.add(SetUserTokenEvent(user: user));
                  return const HomePage();
               }
            }
            return const LoginPage();
         }
        ),
    );
  }
}