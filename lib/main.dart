import 'package:app_indicators/src/blocs/bottom_navigator/bottom_navigator_bloc.dart';
import 'package:app_indicators/src/blocs/dashboard/dashboard_bloc.dart';
import 'package:app_indicators/src/blocs/indicators/indicators_bloc.dart';
import 'package:app_indicators/src/blocs/login/login_bloc.dart';
import 'package:app_indicators/src/blocs/period/period_bloc.dart';
import 'package:app_indicators/src/blocs/worker/worker_bloc.dart';
import 'package:app_indicators/src/loanding.page.dart';
import 'package:app_indicators/src/login.page.dart';
import 'package:app_indicators/src/home.page.dart';
import 'package:app_indicators/src/pages/detail.period.page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sizer/sizer.dart';

void main() => runApp(
  MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => BottomNavigatorBloc()),
        BlocProvider(create: (_) => LoginBloc()),
        BlocProvider(create: (_) => IndicatorsBloc()),
        BlocProvider(create: (_) => WorkerBloc()),
        BlocProvider(create: (_) => PeriodBloc()),
        BlocProvider(create: (_) => DashboardBloc()),
      ],
      child: const MyApp()
    )
);

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {    
    return Sizer(
      builder: (context, orientation, deviceType){
        SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
          statusBarColor: Colors.transparent,
        ));
        return MaterialApp(
          debugShowCheckedModeBanner: false,          
          theme: ThemeData.light().copyWith(
            textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'DHSasns'
            ),            
          ),
          darkTheme: ThemeData.dark().copyWith(
            textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'DHSasns'
            ),            
          ),          
          routes: {
            'login':(context) => const LoginPage(),
            'dashboard': (context) => const HomePage(),
            'home': (context) => const LoandingScreen(),
            'detailPeriod': (context) => const DetailPeriodPage(),
          },
          initialRoute: 'home',
        );
      }
    );
  }
}